### macOS LabVIEW Experiment — Call Library Function

The goal — attempt to create Shared Library, call it via Library Function Node and compare to native LabVIEW code from performance point of view.

#### Used Tools and Dev Environment:

macOS: Sonoma v.14.4.1 (23E224) released 25th March 2024.
LabVIEW 2023 Q3 (64-bit) v.23.3.1f1 (Community Edition) released in Q3 2023.
Xcode v.15.0.1 (15A507) released 18th Oct 2023.
gcc 13.2.0 released April 2023 (if you have Xcode installed, then "own" gcc is already delivered)

Non-mandatory, but recommended:

Vusual Studio Code for Mac v.1.88.1 released 10th Apr 2024.
Far Manager FAR2L v.2.6.1 released 17th Apr 2024 (good "addon" to Finder, like Midnight Commander).

Installation Instructions of these products is out of scope for this experiment.

### Creating Library

There are two different ways how to create Shared Library for macOS. One is - creating *.dylib (dylib is most used, but sometimes *.so or *.bundle as well) with pure gcc and another one is to create *.framework with Xcode. In this experiment both ways will be covered. You must understand the major difference between *.dylib and *.framework: *.dylib is single file, like *.DLL in Windows or *.so in Linux, but *.framework is folder (directory) contains internal information like version, signing certificate etc.

For this exercise you will need gcc and you can check if this installed and which version with following command in Terminal

```
$ gcc -v
```

This is what delivered with Xcode:

![image-20240428171111094](assets/image-20240428171111094.png)

or if you install it with brew then

![image-20240428171244350](assets/image-20240428171244350.png)

below I will use gcc from Xcode.

#### Creating *.dylib for LabVIEW

As a very first exercise we will create simple library from C code.

We will create simple C code with following function:

```c
int fnAdd(int a, int b)
{
	return (a + b);
}
```

I would like to recommend to use Visual Studio code to edit files:

![image-20240428165004354](assets/image-20240428165004354.png)

Save it, for example, as "foo.c". Now we can compile this simple file with following command in the Terminal:

```bash
$ gcc -dynamiclib -o libfoo.dynlib foo.c
```

-dynamiclib obviosly pointed to desired type, -o is output, then output file follow, and foo.c - is our source.
(You can use Visual Studio Code Terminal as well)

Now in LabVIEW use Call Library Function as usually:

![image-20240428165804971](assets/image-20240428165804971.png)

And the result is correct: 2 + 3 = 5. So simple (a little bit easier than in Windows). The only important point: the file name "libfoo.dynlib" as well as Function name ("Add") should be **entered manually**. Selector will not work, and "Library name or path" will always expect directory (you will see how it works with *.framework below).

Another possibility is to create "Bundle", which is the same, but compiled will following command line (and usually the *.so extension given, and sometimes *.bundle):

```bash
$ gcc -bundle -o libfoo.so foo.c
```

This will work exactly so good:

![image-20240428170453730](assets/image-20240428170453730.png)

Traditional Linux way will be also OK:

```
$ gcc -shared -o libfoo.so foo.c
```

Take a note, that the libraries created with -dynamiclib, -bundle and -shared flags are different (but all three will work fine in LabVIEW).

You can check which type behind of the library with command

```
$ otool -hv <YOUR_LIBRARY>
```

![image-20240428172541851](assets/image-20240428172541851.png)

You can see lot of *.so and *.dynlib Libraries in LabVIEW App Folder:

![image-20240428173220232](assets/image-20240428173220232.png)

But if you will take a look into Mean.vi from Math Palette, then you will see something different:

![image-20240428173552620](assets/image-20240428173552620.png)

lvanlys.framework is a Folder, not a File.

#### Creating *.framework for LabVIEW with Xcode

Launch Xcode and select New Project

![image-20240428173825256](assets/image-20240428173825256.png)

on the Next Dialog select Framework:

![image-20240428173908397](assets/image-20240428173908397.png)

on the next screen you will see some options, and the only two languages are offered - Objectie-C and Swift. Don't worry about that, select Objective-C, you can add pure C code later.

![image-20240428175243057](assets/image-20240428175243057.png)

Also disable Tests and Documentation to avoid unnecessary files and save it.

Now create as "New File" (or add) your source file:

![image-20240428175525207](assets/image-20240428175525207.png)

Select C File

![image-20240428175552876](assets/image-20240428175552876.png)

For first exercise you don't need header:

![image-20240428175633151](assets/image-20240428175633151.png)

Here three simple functions:

```c
#include <math.h>

int fnAdd(int a, int b)
{
	return (a+b);
}

double fnPow(double a, double b)
{
	return (pow(a, b));
}


double fnPowR(double a, double b)
{
	return rint((pow(a, b)));
}
```

Now time to build it. How to switch from Debug to Release: Product->Scheme->Edit Scheme...:

![image-20240428202403057](assets/image-20240428202403057.png)

And select Release here:

![image-20240428202505594](assets/image-20240428202505594.png)

Now Build:

![image-20240428202618428](assets/image-20240428202618428.png)

When successful, by default your build will be located here:

```
/Users/user/Library/Developer/Xcode/DerivedData/foo-xxx/Build/Products/Release/foo.framework 
```

(where xxx is unique ID)

Or simply open folder in Finder:

![image-20240428203020064](assets/image-20240428203020064.png)

Now you need to copy this foo.framework folder into your project folder near VI:

![image-20240428205003974](assets/image-20240428205003974.png)

And now you have all "exported" functions available in the list:

![image-20240428203529865](assets/image-20240428203529865.png)

And this method with asterisk will also work:

![image-20240428203705271](assets/image-20240428203705271.png)

(so, in theory universal Mac/Windows/Linux VI with Shared Libraries is possible).

Calling LabVIEW's Memory Manager functions from Shared Lib is out of the scope, probably next time.

### Benchmark Experiment

Benchmark Code:

![image-20240428221555711](assets/image-20240428221555711.png)

Result:

![image-20240428220902629](assets/image-20240428220902629-1714334943368-1.png)

